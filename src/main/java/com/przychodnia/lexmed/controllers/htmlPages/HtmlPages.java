package com.przychodnia.lexmed.controllers.htmlPages;

import com.przychodnia.lexmed.model.Lekarz;
import com.przychodnia.lexmed.model.Pacjent;
import com.przychodnia.lexmed.model.Wizyta;
import com.przychodnia.lexmed.services.LekarzRepo;
import com.przychodnia.lexmed.services.PacjentRepo;
import com.przychodnia.lexmed.services.WizytaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;


@Controller
public class HtmlPages {

    @Autowired
    private LekarzRepo lekarzRepo;

    @Autowired
    private PacjentRepo pacjentRepo;

    @Autowired
    private WizytaRepo wizytaRepo;

    @GetMapping("/index")
    public String index(){
        return "index";
    }

    @GetMapping("/about")
    public String about(){
        return "about";
    }

    @GetMapping("/contact")
    public String contact(){
        return "contact";
    }

    @GetMapping("/pacjenci")
    public String pacjenci(Model model) {
        model.addAttribute("pacjent", new Pacjent());
        model.addAttribute("pacjencii", pacjentRepo.findAll());
        return "pacjenci";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute @Valid Pacjent pacjent, BindingResult result, Model model, HttpServletResponse response) throws IOException {

        if(result.hasErrors()){
            model.addAttribute("pacjent",pacjent);
            return "pacjenci";
        }

        model.addAttribute("pacjent",pacjent);
        pacjentRepo.save(pacjent);

        return "redirect:/pacjenci";
        //response.sendRedirect("/pacjenci");

    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/lekarze")
    public String lekarze(Model model) {
        model.addAttribute("lekarz", new Lekarz());
        model.addAttribute("lekarzee", lekarzRepo.findAll());
        return  "lekarze";
    }

    @PostMapping("/save1")
    public String save1(@ModelAttribute @Valid Lekarz lekarz, BindingResult result, Model model, HttpServletResponse response) throws IOException {

        if(result.hasErrors()){
            model.addAttribute("lekarz",lekarz);
            return "lekarze";
        }

        model.addAttribute("lekarz",lekarz);
        lekarzRepo.save(lekarz);

        return "redirect:/lekarze";
//        response.sendRedirect("/lekarze");
    }

    @GetMapping("/wizyty")
    public  String wizyty(Model model) {
        model.addAttribute("wizyta", new Wizyta());
        model.addAttribute("wizytyy", wizytaRepo.findAll());
        model.addAttribute("pacjenci", pacjentRepo.findAll());
        model.addAttribute("lekarze", lekarzRepo.findAll());
        return  "wizyty";
    }

    @PostMapping("/save2")
    public String save3(@ModelAttribute @Valid Wizyta wizyta, BindingResult result, Model model, HttpServletResponse response) throws IOException {

        if(result.hasErrors()){
            model.addAttribute("wizyta",wizyta);
            return "wizyty";
        }

        model.addAttribute("wizyta",wizyta);
        wizytaRepo.save(wizyta);

        return "redirect:/wizyty";
    }

}
