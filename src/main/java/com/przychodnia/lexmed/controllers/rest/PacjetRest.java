package com.przychodnia.lexmed.controllers.rest;

import com.przychodnia.lexmed.model.Pacjent;
import com.przychodnia.lexmed.services.PacjentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
public class PacjetRest {

    @Autowired
    private PacjentRepo pacjentRepo;

    Pacjent pacjent;

    @GetMapping("/pacjent")
    public List<Pacjent> show() {
        return pacjentRepo.findAll();
    }

    @GetMapping("/pacjent/{id}")
    public Optional<Pacjent> getPacjent(@PathVariable Long id) {
        return pacjentRepo.findById(id);
    }

//    @PostMapping("/pacjent/add")
//    public void save(@ModelAttribute @Valid Pacjent pacjent, BindingResult result, Model model, HttpServletResponse response) throws IOException {
//
//        if(result.hasErrors()){
//            model.addAttribute("pacjent",pacjent);
//            response.sendRedirect("/pacjenci");
//        }
//
//        model.addAttribute("pacjent",pacjent);
//        pacjentRepo.save(pacjent);
//
//        response.sendRedirect("/pacjenci");
//    }

    @GetMapping("pacjentd/{id}")
    public ModelAndView delete(@PathVariable Long id){
        pacjentRepo.deleteById(id);
        return new ModelAndView("redirect:/pacjenci");
    }
}
