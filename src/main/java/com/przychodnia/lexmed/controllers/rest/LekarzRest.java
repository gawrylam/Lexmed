package com.przychodnia.lexmed.controllers.rest;

import com.przychodnia.lexmed.model.Lekarz;
import com.przychodnia.lexmed.services.LekarzRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
public class LekarzRest {

    @Autowired
    private LekarzRepo lekarzRepo;

    @GetMapping("/lekarz")
    public List<Lekarz> show() {
        return lekarzRepo.findAll();
    }


//    @PostMapping("/lekarz/add")
//    public void save(@ModelAttribute @Valid Lekarz lekarz, BindingResult result, Model model, HttpServletResponse response) throws IOException {
//
//        if(result.hasErrors()){
//            model.addAttribute("lekarz",lekarz);
//            response.sendRedirect("/lekarze");
//        }
//
//        model.addAttribute("lekarz",lekarz);
//        lekarzRepo.save(lekarz);
//
//        response.sendRedirect("/lekarze");
//    }

    @GetMapping("lekarz/{id}")
    public ModelAndView delete(@PathVariable Long id){
        lekarzRepo.deleteById(id);
        return new ModelAndView("redirect:/lekarze");
    }
}
