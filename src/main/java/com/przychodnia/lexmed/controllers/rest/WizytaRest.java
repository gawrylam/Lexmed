package com.przychodnia.lexmed.controllers.rest;

import com.przychodnia.lexmed.model.Wizyta;
import com.przychodnia.lexmed.services.WizytaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
public class WizytaRest {

    @Autowired
    private WizytaRepo wizytaRepo;

    @GetMapping("/wizyta")
    public List<Wizyta> show() {
        return wizytaRepo.findAll();
    }

//    @PostMapping("/wizyta/add")
//    public void save(@ModelAttribute @Valid Wizyta wizyta, BindingResult result, Model model, HttpServletResponse response) throws IOException {
//
//        if(result.hasErrors()){
//            model.addAttribute("wizyta",wizyta);
//        }
//
//        model.addAttribute("wizyta",wizyta);
//        wizytaRepo.save(wizyta);
//
//        response.sendRedirect("/wizyty");
//    }

    @GetMapping("wizyta/{id}")
    public ModelAndView delete(@PathVariable Long id){
        wizytaRepo.deleteById(id);
        return new ModelAndView("redirect:/wizyty");
    }
}
