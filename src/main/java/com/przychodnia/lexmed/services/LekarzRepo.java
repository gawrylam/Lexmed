package com.przychodnia.lexmed.services;

import com.przychodnia.lexmed.model.Lekarz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LekarzRepo extends JpaRepository<Lekarz, Long> {
    Lekarz findBySpecialization(String specialization);
}
