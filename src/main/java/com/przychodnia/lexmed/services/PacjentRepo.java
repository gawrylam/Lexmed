package com.przychodnia.lexmed.services;

import com.przychodnia.lexmed.model.Pacjent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PacjentRepo extends JpaRepository<Pacjent, Long> {
    Pacjent findByPesel(Long pesel);
}
