package com.przychodnia.lexmed.services;

import com.przychodnia.lexmed.model.Wizyta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WizytaRepo  extends JpaRepository<Wizyta, Long> {
    Wizyta findByLekarz_Specialization(String specialization);
    Wizyta findByPacjent_Pesel(Long pesel);
}
