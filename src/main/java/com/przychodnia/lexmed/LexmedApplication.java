package com.przychodnia.lexmed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LexmedApplication {

    public static void main(String[] args) {
        SpringApplication.run(LexmedApplication.class, args);
    }
}
