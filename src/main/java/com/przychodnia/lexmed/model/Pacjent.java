package com.przychodnia.lexmed.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;


@Entity
public class Pacjent {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 2, max = 35, message = "Must be between 3 and 35 characters!")
    private String name;

    @NotNull
    @Size(min = 2, max = 35, message = "Must be between 3 and 35 characters!")
    private String lastName;

    @NotNull
    private Long pesel;


    @OneToMany(mappedBy = "pacjent")
    private List<Wizyta> wizyty;

    public Pacjent() {
    }

    public Pacjent(String name, String lastName, Long pesel) {
        this.name = name;
        this.lastName = lastName;
        this.pesel = pesel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getPesel() {
        return pesel;
    }

    public void setPesel(Long pesel) {
        this.pesel = pesel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pacjent)) return false;
        Pacjent pacjent = (Pacjent) o;
        return Objects.equals(getId(), pacjent.getId()) &&
                Objects.equals(getName(), pacjent.getName()) &&
                Objects.equals(getLastName(), pacjent.getLastName()) &&
                Objects.equals(getPesel(), pacjent.getPesel()) &&
                Objects.equals(wizyty, pacjent.wizyty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getLastName(), getPesel(), wizyty);
    }
}
