package com.przychodnia.lexmed.model;

import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
public class Lekarz{
    
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 2, max = 35, message = "Must be between 3 and 35 characters!")
    private String name;

    @NotNull
    @Size(min = 2, max = 35, message = "Must be between 3 and 35 characters!")
    private String lastName;

    @NotNull
    @Size(min = 3, max = 35)
    private String specialization;

    @OneToMany(mappedBy = "lekarz")
    private List<Wizyta> wizyty;

    public Lekarz() {
    }

    public Lekarz(@NotNull @Size(min = 2, max = 35, message = "Must be between 3 and 35 characters!") String name, @NotNull @Size(min = 2, max = 35, message = "Must be between 3 and 35 characters!") String lastName, @NotNull String specialization) {
        this.name = name;
        this.lastName = lastName;
        this.specialization = specialization;
    }

    public List<Wizyta> getWizyty() {
        return wizyty;
    }

    public void setWizyty(List<Wizyta> wizyty) {
        this.wizyty = wizyty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lekarz)) return false;
        Lekarz lekarz = (Lekarz) o;
        return Objects.equals(getId(), lekarz.getId()) &&
                Objects.equals(getName(), lekarz.getName()) &&
                Objects.equals(getLastName(), lekarz.getLastName()) &&
                Objects.equals(getSpecialization(), lekarz.getSpecialization()) &&
                Objects.equals(getWizyty(), lekarz.getWizyty());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getLastName(), getSpecialization(), getWizyty());
    }
}
