package com.przychodnia.lexmed.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "wizyta")
public class Wizyta {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "lekarz_id")
    private Lekarz lekarz;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "pacjent_id")
    private Pacjent pacjent;

    private Date data;

    @NotNull
    private String choroba;

    @NotNull
    private String recepta;

    public Wizyta() {
    }

    public Wizyta( Date data, @NotNull String choroba, @NotNull String recepta) {
        this.data = data;
        this.choroba = choroba;
        this.recepta = recepta;
    }

    public Lekarz getLekarz() {
        return lekarz;
    }

    public void setLekarz(Lekarz lekarz) {
        this.lekarz = lekarz;
    }

    public Pacjent getPacjent() {
        return pacjent;
    }

    public void setPacjent(Pacjent pacjent) {
        this.pacjent = pacjent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getChoroba() {
        return choroba;
    }

    public void setChoroba(String choroba) {
        this.choroba = choroba;
    }

    public String getRecepta() {
        return recepta;
    }

    public void setRecepta(String recepta) {
        this.recepta = recepta;
    }
}
